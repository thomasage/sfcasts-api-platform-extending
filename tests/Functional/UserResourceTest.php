<?php
declare(strict_types=1);

namespace App\Tests\Functional;

use App\Factory\UserFactory;
use App\Test\CustomApiTestCase;
use Ramsey\Uuid\Uuid;

final class UserResourceTest extends CustomApiTestCase
{
    public function testCreateUser(): void
    {
        $client = self::createClient();

        $client->request(
            'POST',
            '/api/users',
            [
                'json' => [
                    'email' => 'cheeseplease@example.com',
                    'username' => 'cheeseplease',
                    'password' => 'brie',
                ],
            ]
        );
        self::assertResponseStatusCodeSame(201);

        $user = UserFactory::repository()->findOneBy(['email' => 'cheeseplease@example.com']);
        self::assertNotNull($user);
        self::assertJsonContains(['@id' => sprintf('/api/users/%s', $user->getUuid())]);

        $this->logIn($client, 'cheeseplease@example.com', 'brie');
    }

    public function testCreateUserWithUuid(): void
    {
        $client = self::createClient();

        $uuid = Uuid::uuid4();

        $client->request(
            'POST',
            '/api/users',
            [
                'json' => [
                    'uuid' => $uuid,
                    'email' => 'cheeseplease@example.com',
                    'username' => 'cheeseplease',
                    'password' => 'brie',
                ],
            ]
        );
        self::assertResponseStatusCodeSame(201);
        self::assertJsonContains(['@id' => sprintf('/api/users/%s', $uuid)]);
    }

    public function testUpdateUser(): void
    {
        $client = self::createClient();
        $user = UserFactory::new()->create();
        $this->logIn($client, $user);

        $client->request(
            'PUT',
            sprintf('/api/users/%s', $user->getUuid()),
            [
                'json' => [
                    'username' => 'newusername',
                    'roles' => ['ROLE_ADMIN'] // will be ignored
                ],
            ]
        );
        self::assertResponseIsSuccessful();
        self::assertJsonContains(['username' => 'newusername']);

        $user->refresh();
        self::assertEquals(['ROLE_USER'], $user->getRoles());
    }

    public function testGetUser(): void
    {
        $client = self::createClient();
        $user = UserFactory::new()->create(
            [
                'phoneNumber' => '555.123.4567',
                'username' => 'cheesehead',
            ]
        );
        $authenticatedUser = UserFactory::new()->create();
        $this->logIn($client, $authenticatedUser);

        $client->request('GET', sprintf('/api/users/%s', $user->getUuid()));
        self::assertResponseStatusCodeSame(200);
        self::assertJsonContains(
            [
                'username' => $user->getUsername(),
                'isMvp' => true,
            ]
        );

        $data = $client->getResponse()->toArray();
        self::assertArrayNotHasKey('phoneNumber', $data);

        // refresh the user & elevate
        $user->refresh();
        $user->setRoles(['ROLE_ADMIN']);
        $user->save();
        $this->logIn($client, $user);

        $client->request('GET', sprintf('/api/users/%s', $user->getUuid()));
        self::assertJsonContains(['phoneNumber' => '555.123.4567']);
    }
}
