<?php
declare(strict_types=1);

namespace App\DataProvider;

use ApiPlatform\Core\DataProvider\PaginatorInterface;
use App\Service\StatsHelper;

final class DailyStatsPaginator implements PaginatorInterface, \IteratorAggregate
{
    private ?\ArrayIterator $dailyStatsIterator = null;
    private StatsHelper $statsHelper;
    private int $currentPage;
    private int $maxResults;
    private ?\DateTimeInterface $fromDate = null;

    public function __construct(StatsHelper $statsHelper, int $currentPage, int $maxResults)
    {
        $this->statsHelper = $statsHelper;
        $this->currentPage = $currentPage;
        $this->maxResults = $maxResults;
    }

    public function count(): int
    {
        return iterator_count($this->getIterator());
    }

    public function getIterator()
    {
        if (null === $this->dailyStatsIterator) {
            $offset = (($this->getCurrentPage() - 1) * $this->getItemsPerPage());
            $this->dailyStatsIterator = new \ArrayIterator(
                $this->statsHelper->fetchMany((int)$this->getItemsPerPage(), (int)$offset, $this->getCriteria())
            );
        }

        return $this->dailyStatsIterator;
    }

    public function getCurrentPage(): float
    {
        return $this->currentPage;
    }

    public function getItemsPerPage(): float
    {
        return $this->maxResults;
    }

    private function getCriteria(): array
    {
        $criteria = [];
        if ($this->fromDate) {
            $criteria['from'] = $this->fromDate;
        }

        return $criteria;
    }

    public function getLastPage(): float
    {
        return ceil($this->getTotalItems() / $this->getItemsPerPage()) ?: 1.;
    }

    public function getTotalItems(): float
    {
        return $this->statsHelper->count();
    }

    public function setFromDate(\DateTimeInterface $fromDate): void
    {
        $this->fromDate = $fromDate;
    }
}
