<?php
declare(strict_types=1);

namespace App\Serializer\Normalizer;

use ApiPlatform\Core\Serializer\AbstractItemNormalizer;
use App\Dto\CheeseListingInput;
use App\Entity\CheeseListing;
use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

final class CheeseListingInputDenormalizer implements DenormalizerInterface, CacheableSupportsMethodInterface
{
    private ObjectNormalizer $normalizer;

    public function __construct(ObjectNormalizer $normalizer)
    {
        $this->normalizer = $normalizer;
    }

    public function denormalize($data, string $type, string $format = null, array $context = [])
    {
        $context[AbstractItemNormalizer::OBJECT_TO_POPULATE] = self::createDto($context);

        return $this->normalizer->denormalize($data, $type, $format, $context);
    }

    private static function createDto(array $context): CheeseListingInput
    {
        $entity = $context[AbstractItemNormalizer::OBJECT_TO_POPULATE] ?? null;
        if ($entity && !$entity instanceof CheeseListing) {
            throw new \RuntimeException(sprintf('Unexpected resource class "%s"', get_class($entity)));
        }

        return CheeseListingInput::createFromEntity($entity);
    }

    public function supportsDenormalization($data, string $type, string $format = null)
    {
        return CheeseListingInput::class === $type;
    }

    public function hasCacheableSupportsMethod(): bool
    {
        return true;
    }
}
