<?php
declare(strict_types=1);

namespace App\Dto;

use App\Entity\CheeseListing;
use App\Entity\User;
use App\Validator\IsValidOwner;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

final class CheeseListingInput
{
    /**
     * @Groups({"cheese:write", "user:write"})
     * @Assert\NotBlank()
     * @Assert\Length(
     *     min=2,
     *     max=50,
     *     maxMessage="Describe your cheese in 50 chars or less"
     * )
     */
    public ?string $title = null;

    /**
     * @Groups({"cheese:write", "user:write"})
     * @Assert\NotBlank()
     */
    public ?int $price = null;

    /**
     * @Groups({"cheese:collection:post"})
     * @IsValidOwner()
     */
    public ?User $owner = null;

    /**
     * @Groups({"cheese:write"})
     */
    public bool $isPublished = false;

    /**
     * @Assert\NotBlank()
     */
    public ?string $description = null;

    public static function createFromEntity(?CheeseListing $cheeseListing): self
    {
        $dto = new self();
        // not an edit, so just return an empty DTO
        if (!$cheeseListing) {
            return $dto;
        }
        $dto->title = (string)$cheeseListing->getTitle();
        $dto->price = (int)$cheeseListing->getPrice();
        $dto->description = (string)$cheeseListing->getDescription();
        $dto->owner = $cheeseListing->getOwner();
        $dto->isPublished = (bool)$cheeseListing->getIsPublished();

        return $dto;
    }

    public function createOrUpdateEntity(?CheeseListing $cheeseListing): CheeseListing
    {
        if (!$cheeseListing) {
            $cheeseListing = new CheeseListing($this->title);
        }
        $cheeseListing
            ->setDescription($this->description)
            ->setIsPublished($this->isPublished)
            ->setPrice($this->price);
        if ($this->owner) {
            $cheeseListing->setOwner($this->owner);
        }

        return $cheeseListing;
    }

    /**
     * The description of the cheese as raw text.
     *
     * @Groups({"cheese:write", "user:write"})
     * @SerializedName("description")
     */
    public function setTextDescription(string $description): self
    {
        $this->description = nl2br($description);

        return $this;
    }
}
