<?php
declare(strict_types=1);

namespace App\Dto;

use App\Entity\CheeseListing;
use App\Entity\User;
use Carbon\Carbon;
use Symfony\Component\Serializer\Annotation\Groups;

final class CheeseListingOutput
{
    /**
     * The title of this listing
     *
     * @Groups({"cheese:read", "user:read"})
     */
    public string $title;

    /**
     * @Groups({"cheese:read"})
     */
    public string $description;

    /**
     * @Groups({"cheese:read"})
     */
    public string $shortDescription;

    /**
     * @Groups({"cheese:read", "user:read"})
     */
    public int $price;

    /**
     * How long ago in text that this cheese listing was added.
     *
     * @Groups({"cheese:read"})
     */
    public string $createdAtAgo;

    /**
     * @Groups({"cheese:read"})
     */
    public User $owner;

    public static function createFromEntity(CheeseListing $cheeseListing): self
    {
        $output = new self();
        $output->title = (string)$cheeseListing->getTitle();
        $output->description = (string)$cheeseListing->getDescription();
        $output->shortDescription = (string)$cheeseListing->getShortDescription();
        $output->price = (int)$cheeseListing->getPrice();
        $output->createdAtAgo = Carbon::instance($cheeseListing->getCreatedAt())->diffForHumans();
        /** @var User $owner */
        $owner = $cheeseListing->getOwner();
        $output->owner = $owner;

        return $output;
    }
}
