<?php
declare(strict_types=1);

namespace App\ApiPlatform;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;

final class CheeseSearchFilter extends AbstractFilter
{
    private const PROPERTY = 'search';
    private bool $useLike;

    public function __construct(
        ManagerRegistry $managerRegistry,
        ?RequestStack $requestStack = null,
        NameConverterInterface $nameConverter = null,
        bool $useLike = false
    ) {
        parent::__construct($managerRegistry, $requestStack, null, null, $nameConverter);
        $this->useLike = $useLike;
    }

    public function getDescription(string $resourceClass): array
    {
        return [
            self::PROPERTY => [
                'property' => null,
                'type' => 'string',
                'required' => false,
                'openapi' => [
                    'description' => 'Search across multiple fields',
                ],
            ],
        ];
    }

    protected function filterProperty(
        string $property,
        $value,
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null
    ) {
        if (self::PROPERTY !== $property) {
            return;
        }
        $alias = $queryBuilder->getRootAliases()[0];
        // A param name that is guaranteed unique in this query
        $valueParameter = $queryNameGenerator->generateParameterName('search');
        $queryBuilder
            ->andWhere(
                sprintf(
                    '%s.title LIKE :%s OR %s.description LIKE :%s',
                    $alias,
                    $valueParameter,
                    $alias,
                    $valueParameter
                )
            )
            ->setParameter($valueParameter, '%'.$value.'%');
    }
}
