<?php
declare(strict_types=1);

namespace App\Doctrine;

use App\Entity\User;

final class UserSetIsMvpListener
{
    public function postLoad(User $user): void
    {
        $user->setIsMvp(false !== strpos($user->getUsername(), 'cheese'));
    }
}
