<?php
declare(strict_types=1);

namespace App\Validator;

use App\Entity\CheeseListing;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

final class ValidIsPublishedValidator extends ConstraintValidator
{
    private $entityManager;

    private $security;

    public function __construct(EntityManagerInterface $entityManager, Security $security)
    {
        $this->entityManager = $entityManager;
        $this->security = $security;
    }

    public function validate($value, Constraint $constraint): void
    {
        /* @var $constraint ValidIsPublished */

        if (!$value instanceof CheeseListing) {
            throw new \LogicException('Only CheeseListing is supported.');
        }

        $originalData = $this->entityManager->getUnitOfWork()->getOriginalEntityData($value);

        $previousIsPublisehd = $originalData['isPublished'] ?? false;

        if ($previousIsPublisehd === $value->getIsPublished()) {
            // isPublished didn't change!
            return;
        }

        // we are publishing
        if ($value->getIsPublished()
            && !$this->security->isGranted('ROLE_ADMIN')
            && strlen($value->getDescription()) < 100) {
            $this->context
                ->buildViolation('Cannot publish: description is too short!')
                ->atPath('description')
                ->addViolation();

        }

        // we are UNpublishing
        if (!$value->getIsPublished()
            && !$this->security->isGranted('ROLE_ADMIN')) {
            $this->context
                ->buildViolation('Only admin users can unpublish.')
                ->addViolation();
        }
    }
}
