<?php
declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\ApiPlatform\DailyStatsDateFilter;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"daily-stats:read"}},
 *     denormalizationContext={"groups"={"daily-stats:write"}},
 *     paginationItemsPerPage=7,
 *     itemOperations={"get", "put"},
 *     collectionOperations={"get"}
 * )
 * @ApiFilter(DailyStatsDateFilter::class, arguments={"throwOnInvalid"=true})
 */
final class DailyStats
{
    /**
     * @Groups({"daily-stats:read"})
     */
    public \DateTimeInterface $date;

    /**
     * @Groups({"daily-stats:read", "daily-stats:write"})
     */
    public int $totalVisitors;

    /**
     * The 5 most popular cheese listings from this date!
     *
     * @var array<CheeseListing>|CheeseListing[]
     * @Groups({"daily-stats:read"})
     */
    public array $mostPopularListings = [];

    /**
     * @param CheeseListing[] $mostPopularListings
     */
    public function __construct(\DateTimeInterface $date, int $totalVisitors, array $mostPopularListings)
    {
        $this->date = $date;
        $this->totalVisitors = $totalVisitors;
        $this->mostPopularListings = $mostPopularListings;
    }

    /**
     * @ApiProperty(identifier=true)
     */
    public function getDateString(): string
    {
        return $this->date->format('Y-m-d');
    }
}
